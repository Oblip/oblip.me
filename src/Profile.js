import React, { Component } from 'react';  
import './App.css'
import { Grid, Row, Col } from 'react-flexbox-grid';
import { BounceLoader } from 'react-spinners';
import QrCode from 'qrcode.react'
import { css } from '@emotion/core';
import NotFound from './components/NotFound'
import {Helmet} from "react-helmet";


const override = css`
    display: flex;
    align-self: center;
    margin: 0 auto;
`;
// oblip.me/:username
export default class Profile extends Component {
  state = {
    account: null,
    isLoading: true,
    notFound: false,
    shouldShowButtons: false,
  }

  componentDidMount() {
    let username = this.props.match.params.username
    this.getUserData(username);
  }

  isMobile () {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
  };

  renderLoader = () => {
    return(
     <Row className="center-md" className="Main-Container">
        <BounceLoader
          css={override}
          sizeUnit={"px"}
          size={80}
          color={'#0084FF'}
          loading={this.state.loading}
        /> 
     </Row>
    )
  }

  renderView = () => {
    if(!this.state.account){
      return <NotFound />
    } else {
      return this.renderMainView()
    }
  }

  getUserData = (username) => {
    let url = `${process.env.REACT_APP_API_URL}/account_availability/${username}`
    fetch(url)
      .then(res => {
       if(res.status !== 200) {
         this.setState({isLoading: false})
       } else {
         return res.json()
       }
      })
      .then(res => {
        this.setState({account: res, isLoading: false})
      }).catch(error => {
        console.log(error)
        this.setState({notFound: true, isLoading: false})
      })
  }

  renderMainView = () => {
    return(
      <Row className="row center-xs">
        <div className="Main-Container">
          <img src='/logo_small.png' id='Logo-Small' alt='oblip qr code logo' />
          <QrCode id="QrCode" value={`http://oblip.me/${this.props.match.params.username}`} size={181} />
          <h2 id="Title">{this.state.account.name}</h2>
          <p id="Subtitle">Pay @{this.props.match.params.username} with Oblip</p>
          {/* <p>{process.env.REACT_APP_API_URL}</p> */}
        </div>
      </Row>
    )
  }

  openApp = (e) => {
    e.preventDefault();
    window.location = `oblip://transaction/${this.props.match.params.username}`

    setTimeout(() => {
      this.setState({shouldShowButtons: true})
    }, 300)
    
    return false;
  }

  render() {

    const isMobile = this.isMobile();

    return (
      <div> 
        <Helmet>
            <title>{`Pay @${this.props.match.params.username} with Oblip`}</title>
            <meta name="description" content={`Scan the code or open the app to pay @${this.props.match.params.username} with Oblip.`} />
            <meta name="og:url" content={`https://oblip.me/${this.props.match.params.username}`} />
            <meta name="og:title" content={`Pay @${this.props.match.params.username} with Oblip`} />
            <meta name="og:description" content={`Scan the code or open the app to pay @${this.props.match.params.username}`} />
            <meta name="og:image" content="/logo.png" />
            <meta name="og:type" content="website" />
            <meta name="fb:app_id" content={`facebook.${process.env.FACEBOOK_APP_ID}`} />
        </Helmet>

        {this.state.isLoading ? this.renderLoader() : this.renderView() }
        {/* { this.renderLoader() } */}
        <div id="Buttons-Section">
          <Col xs={12}>
            { (!isMobile || this.state.shouldShowButtons) && <Row center="xs">
              <a href="https://www.apple.com/ios/app-store/" target="_blank" rel="noopener noreferrer">
                <Col className="Button-Container" xs={3}> 
                  <img src='/apple_icon.png' className="Button-Logo" alt="App Store" />
                  <p className="Button-Text">App Store</p> 
                </Col>
              </a>
              <a href="https://play.google.com/store/apps/" target="_blank" rel="noopener noreferrer">
                <Col  className="Button-Container" xs={3}>
                  <img src='/play_icon.png' className="Button-Logo" alt="Google Play" />
                  <p className="Button-Text">Google Play</p>
                </Col>
              </a>
            </Row>}

            { (isMobile && !this.state.shouldShowButtons) && <Row center="xs">
              <a href="#" onClick={this.openApp} target="_blank" rel="noopener noreferrer">
                <Col  className="Button-Container" xs={3}>
                  <img src='/logo-white.png' id="button-logo" className="Button-Logo" alt="Oblip Logo" />
                  <p className="Button-Text">Open the app</p>
                </Col>
              </a>
            </Row>}
          </Col>
        </div>
      </div>
    );
  }
}
